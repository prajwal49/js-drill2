export function getCityAndCountryOfIndividual(data) {
    if (!Array.isArray(data)) {
        throw new Error("Input must be an array");
    }
    const res = [];
    for (let i = 0; i < data.length; i++) {
        const obj = data[i];
        if (typeof obj !== 'object' || obj === null || !obj.hasOwnProperty('city') || !obj.hasOwnProperty('country')) {
            throw new Error(`Invalid object at index ${i}. Must have 'city' and 'country' properties.`);
        }
        res.push({ city: obj.city, country: obj.country });
    }

    return res;
}
