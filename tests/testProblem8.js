import { arrayOfObjects } from "../data.js";
import { getCityAndCountryOfIndividual } from "../problem8.js";

try {
  console.log(getCityAndCountryOfIndividual(arrayOfObjects));
} catch (error) {
  console.log("Error occured: ", error.message);
}
