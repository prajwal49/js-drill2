export function getEmailOfUser(arrayOfObjects) {
    if (!Array.isArray(arrayOfObjects)) {
        throw new Error("Input must be an array");
    }

    const emails = [];
    for (let i = 0; i < arrayOfObjects.length; i++) {
        const obj = arrayOfObjects[i];
        if (typeof obj !== 'object' || obj === null || !obj.hasOwnProperty('email')) {
            throw new Error(`Invalid object at index ${i}. Must have 'email' property.`);
        }
        emails.push(obj.email);
    }
    return emails;
}
