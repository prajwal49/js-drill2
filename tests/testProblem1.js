import { arrayOfObjects } from "../data.js";
import { getEmailOfUser } from "../problem1.js";

try {
    const emails = getEmailOfUser(arrayOfObjects);
    console.log(emails);
} catch (error) {
    console.error("An error occurred:", error.message);
}
