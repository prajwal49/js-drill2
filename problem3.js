export function studentWhoLiveInAus(data) {
    if (!Array.isArray(data)) {
        throw new Error("Input must be an array");
    }

    const result = [];
    for (let i = 0; i < data.length; i++) {
        const obj = data[i];
        if (typeof obj !== 'object' || obj === null || !obj.hasOwnProperty('isStudent') || !obj.hasOwnProperty('country')) {
            throw new Error(`Invalid object at index ${i}. Must have 'isStudent' and 'country' properties.`);
        }
        if (obj.isStudent && obj.country === 'Australia') {
            result.push(obj.name);
        }
    }

    return result;
}
