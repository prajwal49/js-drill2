export function getHobbiesWithAge(data) {
    if (!Array.isArray(data)) {
        throw new Error("Input must be an array");
    }

    const result = [];
    for (let i = 0; i < data.length; i++) {
        const obj = data[i];
        if (typeof obj !== 'object' || obj === null || !obj.hasOwnProperty('hobbies') || !obj.hasOwnProperty('age')) {
            throw new Error(`Invalid object at index ${i}. Must have 'hobbies' and 'age' properties.`);
        }
        result.push({ hobbies: obj.hobbies, age: obj.age });
    }

    return result;
}
