export function getFirstHobbie(data) {
    if (!Array.isArray(data)) {
        throw new Error("Input must be an array");
    }

    const hobbies = [];
    for (let i = 0; i < data.length; i++) {
        const obj = data[i];
        if (typeof obj !== 'object' || obj === null || !obj.hasOwnProperty('hobbies') || !Array.isArray(obj.hobbies) ) {
            throw new Error(`Invalid object at index ${i}. Must have a non-empty 'hobbies' array.`);
        }
        hobbies.push(obj.hobbies[0]);
    }

    return hobbies;
}
