export function getAgeOfAll(data) {
    if (!Array.isArray(data)) {
        throw new Error("Input must be an array");
    }

    const ages = [];
    for (let i = 0; i < data.length; i++) {
        const obj = data[i];
        if (typeof obj !== 'object' || obj === null || !obj.hasOwnProperty('name') || !obj.hasOwnProperty('age')) {
            throw new Error(`Invalid object at index ${i}. Must have 'name' and 'age' properties.`);
        }
        ages.push({ name: obj.name, age: obj.age });
    }

    return ages;
}
