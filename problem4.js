export function accessNameAndCity(data, idx) {
    if (!Array.isArray(data)) {
        throw new Error("Input must be an array");
    }

    if (idx < 0 || idx >= data.length) {
        throw new Error(`Index ${idx} is out of bounds for the given array`);
    }

    const result = [];
    result.push({ name: data[idx].name, city: data[idx].city });

    return result;
}
