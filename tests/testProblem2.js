import { arrayOfObjects } from "../data.js";
import { getHobbiesWithAge } from "../problem2.js";

try {
    const hobbiesAndAges = getHobbiesWithAge(arrayOfObjects);
    console.log(hobbiesAndAges);
} catch (error) {
    console.error("An error occurred:", error.message);
}
