import { arrayOfObjects } from "../data.js";
import { getAgeOfAll } from "../problem5.js";

try {
    const ages = getAgeOfAll(arrayOfObjects);
    console.log(ages);
} catch (error) {
    console.error("An error occurred:", error.message);
}
