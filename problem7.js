export function getIndividualsByAge(data, age) {
    if (!Array.isArray(data)) {
        throw new Error("Input must be an array");
    }

    const res = [];
    for (let i = 0; i < data.length; i++) {
        const obj = data[i];
        if (typeof obj !== 'object' || obj === null || !obj.hasOwnProperty('name') || !obj.hasOwnProperty('email') || !obj.hasOwnProperty('age')) {
            throw new Error(`Invalid object at index ${i}. Must have 'name', 'email', and 'age' properties.`);
        }
        if (obj.age === age) {
            res.push({ name: obj.name, email: obj.email });
        }
    }

    return res;
}
