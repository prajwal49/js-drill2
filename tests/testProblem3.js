import { arrayOfObjects } from "../data.js";
import { studentWhoLiveInAus } from "../problem3.js";

try {
    const studentsInAus = studentWhoLiveInAus(arrayOfObjects);
    console.log(studentsInAus);
} catch (error) {
    console.error("An error occurred:", error.message);
}
